/**
 * Created by sammy on 1/21/18.
 */
var express = require("express");
var router = express.Router();

router.get("/test", function(req, res) {
    res.send("you are authenticated!");
});
module.exports = router;