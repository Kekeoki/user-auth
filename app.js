require('dotenv').config();
/**
 * Created by sammy on 1/21/18.
 */
var express = require("express");
var app = express();
var bodyParser = require('body-parser');
var exphbs = require("express-handlebars");
var models = require("./app_api/models");
var morgan = require('morgan');
var authRoutes = require("./app_api/routes/auth.routes");
var apiRoutes = require("./app_api/routes/api.routes");
var jwt = require("express-jwt");

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
// parse application/json
app.use(bodyParser.json());

// set up logger
app.use(morgan('dev'));

//set up view engine
app.engine('handlebars', exphbs({defaultLayout: 'main',
    layoutsDir: __dirname + '/app_server/views/layout'}));
app.set('view engine', 'handlebars');
app.set('views', __dirname + '/app_server/views');
// SET UP ENV
var port = process.env.PORT || 3000;
var isDev = process.env.NODE_ENV === 'development';

app.get("/", function(req,res) {
    res.render("index");
});

app.use("/auth", authRoutes);

var auth = jwt({
    secret: process.env.JWT_SECRET,
    userProperty: 'payload'
});
app.use(auth);

app.use("/api", apiRoutes);

// SYNC AND START SERVERS
models.sequelize.sync({force: isDev})
.then(function() {
    app.listen(port, function() {
        console.log("listening on port: " + port);
    });
})
.catch(function(err) {
    console.error(err);
    throw err;
});
module.exports = app;
